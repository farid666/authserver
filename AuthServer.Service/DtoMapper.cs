﻿using AuthServer.Core.Dtos;
using AuthServer.Core.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthServer.Service
{
    class DtoMapper:Profile
    {
        public DtoMapper()
        {
            CreateMap<ProductDto, Product>().ReverseMap(); //reverseMap hem productdan dto ya hem eksine map etmek olur
            CreateMap<UserAppDto, AppUser>().ReverseMap();
        }
    }
}
