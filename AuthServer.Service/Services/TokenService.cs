﻿using AuthServer.Core.Conf;
using AuthServer.Core.Dtos;
using AuthServer.Core.Models;
using AuthServer.Core.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using SharedLibrary.Configuration;
using SharedLibrary.Services;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AuthServer.Service.Services
{
    public class TokenService : ITokenService
    {

        private readonly UserManager<AppUser> _userManager;
        private readonly CustomTokenOptions _customTokenOptions;

        public TokenService(UserManager<AppUser> userManager,IOptions<CustomTokenOptions> options)
        {
            _userManager = userManager;
            _customTokenOptions = options.Value;
        }


        private string CreateRefreshToken()
        {
            var numberByte = new Byte[32];

            using var rnd = RandomNumberGenerator.Create();

            rnd.GetBytes(numberByte);

            return Convert.ToBase64String(numberByte);
        }

        private IEnumerable<Claim> GetClaims(AppUser appUser,List<String> audiences)
        {
            var userList = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier,appUser.Id),
                new Claim(JwtRegisteredClaimNames.Email,appUser.Email),
                new Claim(ClaimTypes.Name,appUser.UserName),
                new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString()) // bu sadece bu claime id verirem
            };

            userList.AddRange(audiences.Select(x => new Claim(JwtRegisteredClaimNames.Aud, x))); //tokenin hansi apilere accesi oldugunu yoxluyur

            return userList;
        }

        private IEnumerable<Claim> GetClaimsByClient(Client client) //bu Claimler login teleb etmiyen apileri tokennen qorumaq ucun yaradilir bununla muraciet edilir
        {
            var claims = new List<Claim>();
            claims.AddRange(client.Audiences.Select(x => new Claim(JwtRegisteredClaimNames.Aud, x)));
            claims.Add(new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString()));
            claims.Add(new Claim(JwtRegisteredClaimNames.Sub,client.Id.ToString()));

            return claims;
        }

        public TokenDto CreateToken(AppUser appUser)
        {
            var accessTokenExpireDate = DateTime.Now.AddMinutes(_customTokenOptions.AccessTokenExpireDate);
            var refreshTokenExpireDate = DateTime.Now.AddMinutes(_customTokenOptions.RefreshTokenExpireDate);

            var securityKey = SignService.GetSymmetricSecurityKey(_customTokenOptions.SecurityKey);

            SigningCredentials signingCredentials = new SigningCredentials(securityKey,SecurityAlgorithms.HmacSha256Signature);

            JwtSecurityToken jwtSecurityToken = new JwtSecurityToken(
                issuer:_customTokenOptions.Issuer
                ,expires:accessTokenExpireDate
                ,notBefore:DateTime.Now
                ,claims:GetClaims(appUser,_customTokenOptions.Audiences)
                ,signingCredentials:signingCredentials);

            var tokenHandler = new JwtSecurityTokenHandler();

            var token = tokenHandler.WriteToken(jwtSecurityToken);

            var tokenDto = new TokenDto
            {
               AccessToken = token,
               RefreshToken = CreateRefreshToken(),
               AccessTokenExpireDate = accessTokenExpireDate,
               RefreshTokenExpireDate = refreshTokenExpireDate
            };

            return tokenDto;

        }

        public ClientTokenDto CreateTokenByClient(Client client)
        {
            var accessTokenExpireDate = DateTime.Now.AddMinutes(_customTokenOptions.AccessTokenExpireDate);

            var securityKey = SignService.GetSymmetricSecurityKey(_customTokenOptions.SecurityKey);

            SigningCredentials signingCredentials = new SigningCredentials(securityKey,SecurityAlgorithms.HmacSha256Signature);

            JwtSecurityToken jwtSecurityToken = new JwtSecurityToken(
                issuer:_customTokenOptions.Issuer
                ,expires:accessTokenExpireDate
                ,notBefore:DateTime.Now
                ,claims:GetClaimsByClient(client)
                ,signingCredentials:signingCredentials);

            var handler = new JwtSecurityTokenHandler();

            var token = handler.WriteToken(jwtSecurityToken);

            var clientTokenDto = new ClientTokenDto
            {
                AccessToken = token,
                AccessTokenExpireDate = accessTokenExpireDate
            };

            return clientTokenDto;
        }
    }
}
