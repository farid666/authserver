﻿using AuthServer.Core.Conf;
using AuthServer.Core.Dtos;
using AuthServer.Core.Models;
using AuthServer.Core.Repositories;
using AuthServer.Core.Services;
using AuthServer.Core.UnitOfWork;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SharedLibrary.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthServer.Service.Services
{
    public class AuthenticationService : IAuthenticationService
    {

        private readonly ITokenService _tokenService;
        private readonly UserManager<AppUser> _userManager;
        private readonly List<Client> _clients;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IGenericRepository<UserRefreshToken> _genericUserRefreshTokenRepository;


        public AuthenticationService(ITokenService tokenService,UserManager<AppUser> userManager,IOptions<List<Client>> clients
            ,IUnitOfWork unitOfWork,IGenericRepository<UserRefreshToken> genericUserRefreshTokenRepository)
        {
            _clients = clients.Value;
            _userManager = userManager;
            _unitOfWork = unitOfWork;
            _tokenService = tokenService;
            _genericUserRefreshTokenRepository = genericUserRefreshTokenRepository;
        }



        public async Task<Response<TokenDto>> CreateTokenAsync(LoginDto loginDto)
        {
            if (loginDto == null)
            {
                throw new ArgumentNullException(nameof(loginDto));
            }

            var user = await _userManager.FindByEmailAsync(loginDto.Email);

            if (user == null)
            {
                return Response<TokenDto>.Fail("Email or Password invalid!",400,true);
            }

            if (!await _userManager.CheckPasswordAsync(user,loginDto.Password))
            {
                return Response<TokenDto>.Fail("Email or Password invalid!", 400, true);
            }

            var token = _tokenService.CreateToken(user);

            var userRefreshToken = _genericUserRefreshTokenRepository.Where(x => x.UserId == user.Id).SingleOrDefault();

            if (userRefreshToken == null)
            {
                await _genericUserRefreshTokenRepository.AddAsync(new UserRefreshToken {UserId = user.Id,RefreshToken = token.RefreshToken,ExpireDate = token.RefreshTokenExpireDate });

            }
            else
            {
                userRefreshToken.RefreshToken = token.RefreshToken;
                userRefreshToken.ExpireDate = token.RefreshTokenExpireDate;
            }

            await _unitOfWork.CommitAsync();

            return Response<TokenDto>.Success(token,200);
        }

        public Response<ClientTokenDto> CreateTokenByClient(ClientLoginDto clientLoginDto)
        {
            var client = _clients.SingleOrDefault(x=>x.Id == clientLoginDto.Id && x.Secret == clientLoginDto.Secret);

            if (client == null)
            {
                return Response<ClientTokenDto>.Fail("Client Id or Client Secret not found!",404,true);
            }

            var token = _tokenService.CreateTokenByClient(client);

            return Response<ClientTokenDto>.Success(token,200);
        }

        public async Task<Response<TokenDto>> CreateTokenByRefreshTokenAysnc(string refreshToken)
        {
            var existRefreshToken = await _genericUserRefreshTokenRepository.Where(x=>x.RefreshToken == refreshToken).SingleOrDefaultAsync();

            if (existRefreshToken == null)
            {
                return Response<TokenDto>.Fail("Refresh Token not found",404,true);
            }

            var user = await _userManager.FindByIdAsync(existRefreshToken.UserId);

            if (user == null)
            {
                return Response<TokenDto>.Fail("User Id not found", 404, true);
            }

            var tokenDto = _tokenService.CreateToken(user);

            existRefreshToken.RefreshToken = tokenDto.RefreshToken;
            existRefreshToken.ExpireDate = tokenDto.RefreshTokenExpireDate;

            await _unitOfWork.CommitAsync();

            return Response<TokenDto>.Success(tokenDto,200);
        }

        public async Task<Response<NoDataDto>> RevokeRefreshToken(string refreshToken)
        {
            var existRefreshToken =  await _genericUserRefreshTokenRepository.Where(x=>x.RefreshToken == refreshToken).SingleOrDefaultAsync();

            if (existRefreshToken == null)
            {
                return Response<NoDataDto>.Fail("Refresh Token not found!",404,true);
            }

            _genericUserRefreshTokenRepository.Remove(existRefreshToken);

            await _unitOfWork.CommitAsync();

            return Response<NoDataDto>.Success(200);
        }
    }
}
