﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthServer.Service
{
    public static class ObjectMapper
    {
        //lazy metodu gec initialize edir ne vaxt lazim olsa onda cagririr proqram ise dusende cagirmir memoryni bos yere zanit etmirik
        private static readonly Lazy<IMapper> lazy = new Lazy<IMapper>(() => 
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<DtoMapper>();
            });

            return config.CreateMapper();
        });

        public static IMapper Mapper => lazy.Value; 
    }
}
