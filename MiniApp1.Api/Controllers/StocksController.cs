﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace MiniApp1.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class StocksController : ControllerBase
    {

        [HttpGet]
        public IActionResult GetStock()
        {
            var userName = User.Identity.Name;
            var userIdClaim = User.Claims.Where(x => x.Type == ClaimTypes.NameIdentifier).FirstOrDefault();

            return Ok($"Stock : UserName = {userName} - UserId = {userIdClaim.Value}");
        }
    }
}
