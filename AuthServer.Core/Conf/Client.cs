﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthServer.Core.Conf
{
    public class Client
    {
        public string Id { get; set; }
        public string Secret { get; set; }

        //www.myapi1.com www.myapi2.com  bulara misal acces veriremki cagira bilsin
        public List<String> Audiences { get; set; } // bu hansi apilere bu clientin accessi olsun deye gosderirirem

    }
}
