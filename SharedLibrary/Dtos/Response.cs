﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace SharedLibrary.Dtos
{
    public class Response<T> where T:class
    {
        public T Data { get; private set; }

        public int StatusCode { get; private set; }

        [JsonIgnore]
        public bool IsSuccessfully { get; set; } //daxilde ozum gormek ucun yazdim

        public ErrorDto Error { get; private set; }


        public static Response<T> Success(T data,int statusCode)
        {
            return new Response<T> {Data = data,StatusCode = statusCode,IsSuccessfully = true };
        }

        public static Response<T> Success(int statusCode)
        {
            return new Response<T> { Data = default, StatusCode = statusCode, IsSuccessfully = true };
        }

        public static Response<T> Fail(ErrorDto errorDto,int statusCode)
        {
            return new Response<T> {Error = errorDto,StatusCode = statusCode,IsSuccessfully = false };
        }

        public static Response<T> Fail(string error, int statusCode,bool isShow)
        {

            var errorDto = new ErrorDto(error,isShow);
            return new Response<T> { Error = errorDto, StatusCode = statusCode, IsSuccessfully = false };
        }

    }
}
